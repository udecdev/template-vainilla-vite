const PDFViewerUdeC = {
	setup: function (config, onScrollEnd) {
	  pdfjsLib.getDocument(config.url).promise.then(pdf => {
		const container = document.getElementById(config.containerId);
		for (let pageNum = 1; pageNum <= pdf.numPages; pageNum++) {
		  pdf.getPage(pageNum).then(page => {
			const canvas = document.createElement('canvas');
			const context = canvas.getContext('2d');
			const viewport = page.getViewport({ scale: 1 });
			canvas.height = viewport.height;
			canvas.width = viewport.width;
			const renderContext = {
			  canvasContext: context,
			  viewport: viewport
			};
			container.appendChild(canvas);
			page.render(renderContext);
		  });
		}
		container.addEventListener('scroll', function () {
		  const scrolledToBottom = (container.scrollHeight - container.scrollTop) - 200 <= container.clientHeight;
		  if (scrolledToBottom) {
			if (onScrollEnd) {
			  onScrollEnd();
			}
		  }
		});
	  });
	}
  };

export default PDFViewerUdeC;  



