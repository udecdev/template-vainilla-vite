// Crear un archivo llamado YouTubePlayerUdec.js
const YouTubePlayerUdec = (video, callback) => {
	let player;
	let tag = document.createElement('script');
	tag.src = 'https://www.youtube.com/iframe_api';
	let firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	let videoId = extractVideoId(video.url);
	window.onYouTubeIframeAPIReady = () => {
	  console.log("entro");
	  player = new YT.Player(video.id, {
		height: '390',
		width: '640',
		videoId: videoId,
		events: {
		  'onReady': onPlayerReady,
		  'onStateChange': onPlayerStateChange
		}
	  });
	};
	function onPlayerReady(event) {
	  //player.playVideo();
	}
	function onPlayerStateChange(event) {
	  if (event.data === YT.PlayerState.PLAYING) {
		const duration = player.getDuration();
		const ninetyPercent = duration * 0.9;
  
		let timer = setInterval(() => {
		  const currentTime = player.getCurrentTime();
		  console.log('Tiempo actual: ' + currentTime);
		  if (currentTime >= ninetyPercent) {
			callback();
			clearInterval(timer);
		  }
		}, 1000); // Verifica cada segundo
	  }
	}
	function extractVideoId(url) {
	  let videoId = '';
	  if (url.includes('youtube.com') || url.includes('youtu.be')) {
		videoId = url.split(/v\/|v=|youtu\.be\//)[1];
		const ampersandPosition = videoId.indexOf('&');
		if (ampersandPosition !== -1) {
		  videoId = videoId.substring(0, ampersandPosition);
		}
	  }
	  console.log('ID del video: ' + videoId);
	  return videoId;
	}
  };
  export default YouTubePlayerUdec;
  