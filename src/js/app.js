import { SCORM } from 'pipwerks-scorm-api-wrapper';
import PDFViewerUdeC from './PDFViewerUdec';
window.onload = function () {
	// Código a ejecutar cuando se ha cargado toda la página
	document.body.style.visibility = "visible";
	// stage2.stop();
	main();

};

var audios = [{
	url: "./sounds/click.mp3",
	name: "clic"
}];
var limitSelect = 0;
var optionSelectConcepto = "";
var optionSelect = ["", "", "", ""];
ivo.info({
	title: "Universidad de Cundinamarca",
	autor: "Edilson Laverde Molina",
	date: "",
	email: "edilsonlaverde_182@hotmail.com",
	icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
	facebook: "https://www.facebook.com/edilson.laverde.molina"
});


function main() {
	var t = null;
	var udec = ivo.structure({
		created: function () {
			t = this;
			t.animations();
			t.events();
			t.init();
			//precarga audios//
			var onComplete = function () {
				ivo("#preload").hide();
			};
			ivo.load_audio(audios, onComplete);
		},
		methods: {
			init: function () {
				let _this = this;
					//evento que detecta cuando van cerrar la ventana
				window.onbeforeunload = function () {
					// Puedes realizar acciones específicas aquí
					setTimeout(function() {
					
					}, 500); // Ejemplo de espera de 500 milisegundos
				
				};
			},
			events: function () {
				var t = this;
			},
			animations: function () {
				let movil = false;
				let windowWidth = window.innerWidth;
				if (windowWidth < 1024) {
					movil = true;
				}
				if ('ontouchstart' in window || navigator.maxTouchPoints) {
					movil = true;
				}
				if (movil) {
				}
			}
		}
	});
}