import { defineConfig } from 'vite';
import sass from 'sass';

export default defineConfig({
  base:'./',
  server: {
    host: '0.0.0.0' // Esta es la configuración importante para permitir acceso desde otros dispositivos en la red local
  },
  plugins: [
    // procesar archivos scss
    {
      name: 'sass',
      plugin: async () => {
        const result = await sass.render({
          // opciones de compilación de Sass
          // puedes personalizar esto según tus necesidades
          includePaths: ['node_modules'],
          outputStyle: 'compressed'
        });
        return {
          transform: 'style',
          code: result.css.toString()
        };
      }
    }
  ],
  build: {
    // establecer nombres de archivo fijos para la construcción
    rollupOptions: {
      output: {
        entryFileNames: 'app.js',
        chunkFileNames: 'app-[name].js',
        assetFileNames: 'app-[name].[ext]'
      }
    }
  }
});
